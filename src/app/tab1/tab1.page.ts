import { Component } from '@angular/core';
import { ModelosApi } from '../Modelos/ModelosApi';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor() {}
  mensajeDeBienvenida: string;
  url: string;
  datosDeRespuesta: ModelosApi.datosSW;
  procesando: boolean;

  ngOnInit(){
    this.mensajeDeBienvenida = "Bienvenido a mi ap";
    this.url = "https://swapi.dev/api/people/";
    this.datosDeRespuesta = <ModelosApi.datosSW>{};
    this.procesando = false;
  }

  clickBoton(){
    this.procesando = true;
    fetch(this.url)
    .then(response =>
      // debugger;
      response.json()
      // var res = response;
    )
    .then((datos: ModelosApi.datosSW) => {
      debugger;
      this.datosDeRespuesta = datos;
      this.procesando = false;
    });
  }

}
